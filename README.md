Usar o npm por enquanto
Alterar para português
- Iniciando projeto
npm init ou npm init -y (caso queira responder tudo yes)
Obs: caso o projeto já exista, use "npm install" para baixar os pacotes

- code .

- Instalando framework express
npm i express

- Instalando o Typescript
npm i typescript --save-dev

- Cria o arquivo tsconfig.json (arquivo que armazena como as configurações de como
o typescript será executado)
npx tsconfig-init

- Criar a pasta src (Nota: Os arquivos de código sempre ficam na pasta src)
- Dentro da pasta src, crie o arquivo server.ts
- No arquivo tsconfig.json setar o rootDir para "./src"
Ex: "rootDir": "./src",

- Setar o diretório de distribuição que armazena o build, o outDir para "./dist"
Ex: "outDir": "./dist",

Você verifica quando executa:
npm run tsc

Em dist será criado o server.js oriundo de server.ts

Comece a codar no server.ts em que você já terá a última versão disponível do javascript
com as features mais recentes da linguagem

por exemplo, ao codar:
import express from 'express';

Você percebe que o express provavelmente vai estar em vermelho indicando que não está instalado, daí:
npm i @types/express --save-dev

Faça isso com toda lib que você tiver esse problema

ATENÇÂO: Você só faz esse tipo de procedimento em desenvolvimento, já que em produção só existirá código em javascript (dist)

Sempre que terminar de codar ou fazer um bloco, execute npm tsc, pois o node não entende typescript, somente javascript

Depois de tudo, execute:
node dist/server.js

Para agilizar, vá em package.json e crie um script para evitar escrever "npx tsc" e executar logo:
"scripts": {
    "build": "tsc"
},

daí, basta aplicar:
npm run build

Somente em desenvolvimento:
npm i ts-node-dev --save-dev

Com esse módulo, ele permite uma agilidade tirando a criação do diretório dist em dev e restart no  servidor quando houver alteração de código (semelhante ao nodemon):
npm run dev:server

Algumas modificações para tornar o servidor mais rápido em dev:
--transpile-only no package.json (não verifica se o código está certo ou errado, só converte desabilitando a checagem, pois já basta o vscode para isso)
--ignore-watch node_modules no package.json (evita compilação dos módulos em node_modules, pois normalmente nunca alteramos código dos node_modules)
Obs: A percepção fica conforme aumento do projeto

----------------------------
Próximo avanço:
Dividir o arquivo de rotas de acordo com cada entidade da aplicação

----------------------
Padrão Repositório
// Persistência <-> Repositório <-> Rota

// find
// create
// Normalmente um repositório por model

Padrão DTO - Data Transfer Object

instalando o uuid
npm install uuid
npm i --save-dev @types/uuid

Para iniciar o servidor:

npm run dev:server
------
Instalando Testes 
npm install jest -D
npm install ts-jest -D
npm install @types/jest -D
criar o arquivo jest.config.js com o preset correto.
npx ts-jest config:init

Executar os testes:
npm test
------------------
Lista reservas:
curl --location 'localhost:3333/reservas'
Cria reserva:
curl --request POST --location 'localhost:3333/reservas' --header 'Content-Type: application/json' --data '{"agente": "Valdir Torres B","data":"2023-02-05T22:00:00.000Z"}'
------------------

docker exec -it roteiro-projeto-node-myservice-1 /bin/bash

NA AWS:
apigateway:
roteiros-dev (o4cqq6i4cc)

curl --location 'https://o4cqq6i4cc.execute-api.sa-east-1.amazonaws.com/dev/roteiro-projeto-node/reservas'

curl --location 'https://o4cqq6i4cc.execute-api.sa-east-1.amazonaws.com/dev/roteiro-projeto-node/reservas' \
--header 'Content-Type: application/json' \
--data '{
    "agente": "Valdir Torres Borges",
    "data": "2023-10-10T11:00:00.000Z"
}'
-------------
Nome do cluster:
Em dev:
utils-dev
Em produção:
utils-prd















