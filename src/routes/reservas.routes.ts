import { Router } from 'express';
import { parseISO } from 'date-fns';

import ReservaRepository from '../repositories/ReservaRepository';
import CriarReservaService from '../services/CriarReservaService';

const reservasRouter = Router();
const reservaRepository = new ReservaRepository();

// SoC: Separation of Concerns (Separação de preocupações)
// Rota: Receber a requisição, chamar outro arquivo, devolver uma resposta

reservasRouter.get('/', (request, response) => {
    const Reservas = reservaRepository.all();

    return response.json(Reservas);
});

reservasRouter.post('/', (request, response) => {
    try {
        const { agente, data } = request.body;

        const dataParseada = parseISO(data);

        const criarReserva = new CriarReservaService(
            reservaRepository,
        );

        const reserva = criarReserva.execute({
            data: dataParseada,
            agente,
        });

        return response.json(reserva);
    } catch (err) {
        if (err instanceof Error) {
            return response.status(400).json({ error: err.message });
        }
    }
});

export default reservasRouter;
