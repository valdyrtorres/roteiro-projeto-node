import { Router } from 'express';
import reservasRouter from './reservas.routes';

const routes = Router();

// define rota raiz
routes.use('/reservas', reservasRouter);

export default routes;
