import { startOfHour } from 'date-fns';
import Reserva from '../models/Reserva';
import ReservaRepository from '../repositories/ReservaRepository';

/**
 * [x] Recebimento das informações
 * [x] Tratativa de erros/exceções
 * [x] Acesso ao repositório
 */

// DTO
interface Request {
    agente: string;
    data: Date;
}

/**
 * SOLID
 * Single Responsability Principle
 * Dependency Inversion Principle
 */

// O service tem que ter uma única responsabilidade
// DRY: Don't repeat yourself
export class CriarReservaService {
    private reservaRepository: ReservaRepository;

    constructor(reservaRepository: ReservaRepository) {
        this.reservaRepository = reservaRepository;
    }

    public execute({ data, agente }: Request): Reserva {
        // Regra de negócio
        const reservaData = startOfHour(data);

        const findReservaNaMesmaData =
            this.reservaRepository.findByData(reservaData);

        if (findReservaNaMesmaData) {
            throw Error('Esta reserva já está agendada');
        }

        const reserva = this.reservaRepository.create({
            agente,
            data: reservaData,
        });

        return reserva;
    }
}

export default CriarReservaService;
