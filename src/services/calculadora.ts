export class Calculadora {
    value: number 

    constructor() {
        this.value = 0
    }

    soma(n: number): number {
        this.value += n
        return this.value
    }
}