import { v4 as uuidv4 } from 'uuid';

class Reserva {
    id: string;

    agente: string;

    data: Date;

    constructor({ agente, data }: Omit<Reserva, 'id'>) {
        this.id = uuidv4();
        this.agente = agente;
        this.data = data;
    }
}

export default Reserva;
