import { isEqual } from 'date-fns';
import Reserva from '../models/Reserva';

// Data Transfer Object
interface CriarReservaDTO {
    agente: string;
    data: Date;
}

class ReservaRepository {
    private reservas: Reserva[];

    constructor() {
        this.reservas = [];
    }

    public all(): Reserva[] {
        return this.reservas;
    }

    public findByData(data: Date): Reserva | null {
        const findReserva = this.reservas.find(reserva =>
            isEqual(data, reserva.data),
        );

        return findReserva || null;
    }

    public create({ agente, data }: CriarReservaDTO): Reserva {
        const reserva = new Reserva({ agente, data});

        this.reservas.push(reserva);

        return reserva;
    }
}

export default ReservaRepository;
