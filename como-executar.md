Relembrando: Para iniciar o servidor em dev:

npm run dev:server

Para executar os testes: npm run test (ver projeto)

npm test

via curl Criar

curl --location --request POST 'localhost:3333/reservas' \
--header 'Content-Type: application/json' \
--data-raw '{
    "agente": "Valdir Torres",
    "data": "2023-01-04T22:00:00.000Z"
}'

Listar

curl --location --request GET 'localhost:3333/reservas'

-------------------------------------------------------------

Como rodar a cobertura:

Instalar o c8
npm i --save-dev c8

no package.json incluir:
"coverage": "c8 --check-coverage npm run test",
"cover100": "c8 --100 --check-coverage npm run test"

este último comando verifica se houve 100% de cobertura
Para executar no CLI (powershel, cmd, etc)
npm run coverage
npm run cover100