FROM  node:18.12.1

WORKDIR /usr/src/app

COPY . .
ENV TZ=America/Sao_Paulo 

RUN npm install 
RUN npm run build
RUN npm run test

EXPOSE 8081
CMD ["node", "./dist/src/server.js"]
