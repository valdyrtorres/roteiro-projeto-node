import { Calculadora } from '../../src/services/calculadora'

describe('calculadora', () => {
    it('deve inicializar a calculadora com valor 0', () => {
        let calculadora = new Calculadora()
        expect(calculadora.value).toBe(0)
    })

    it('deve retornar um resultado de 5 ao somar 5 imediatamente após a criação', () => {
        let calculadora = new Calculadora()
        expect(calculadora.soma(5)).toBe(5)
    })

    it('deve armazenar o resultado de uma soma realizada imediatamente após a criação como o novo valor calculado', () => {
        let calculadora = new Calculadora()
        calculadora.soma(9)
        expect(calculadora.value).toBe(9)
    })

    it('deve permitir que múltiplas somas sejam realizadas, retornando o resultado da soma final', () => {
        let calculadora = new Calculadora()
        calculadora.soma(9)
        calculadora.soma(2)
        expect(calculadora.soma(1)).toBe(12)
    })

    it('deve armazenar o resultado de várias somas como o novo valor calculado', () => {
        let calculadora = new Calculadora()
        calculadora.soma(5)
        calculadora.soma(7)
        calculadora.soma(3)
        expect(calculadora.value).toBe(15)
    }) 
})

