import { CriarReservaService } from '../../src/services/CriarReservaService'
import { parseISO } from 'date-fns';
import Reserva from '../../src/models/Reserva';
import ReservaRepository from '../../src/repositories/ReservaRepository';

describe('CriarReservaService', () => {
    it('deve criar uma reserva', () => {
        const reservaRepository = new ReservaRepository();
        let criarReserva = new CriarReservaService(reservaRepository)

        const dataParseada = parseISO("2023-01-04T22:00:00.000Z");

        const agente = "Marcel Proust";

        let reserva:Reserva = criarReserva.execute({
            data: dataParseada,
            agente,
        });

        expect(reserva.id).not.toBe(null)
    })
})